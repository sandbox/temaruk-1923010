(function ($) {
  Drupal.behaviors.zenForm = {
    attach:function (context, settings) {
      // Add zen for all supported elements in form.
      $('form.zen-mode', context).once('zen-form-plugin', function () {
        var thisForm = $(this);

        // Create element that opens zen mode.
        thisForm.prepend('<a href="#" class="go-zen">' + Drupal.settings.zenForm.goButtonText + '</a>');
        thisForm.zenForm();
      });

      // todo: Add zen mode for single element.
    }
  }
})(jQuery);
