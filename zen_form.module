<?php
/**
 * @file
 * Makes the 'Zen Form' plugin available to Drupal as a library.
 */

/**
 * Implements hook_libraries_info().
 */
function zen_form_libraries_info() {
  $libraries['zen_form_plugin'] = array(
    'name' => 'Zen Forms',
    'vendor url' => 'https://github.com/Temaruk/zen-form/tree/feature/jquery-support-pre-1.7',
    'download url' => 'https://github.com/Temaruk/zen-form/tree/feature/jquery-support-pre-1.7',
    'version arguments' => array(
      'file' => 'zen-form.js',
      // 1.x: Zen Forms 1.0.2
      'pattern' => '/Zen\s+Forms\s+([0-9\.]+)/',
      'lines' => 2,
      'cols' => 30,
    ),
    'files' => array(
      'js' => array(
        'zen-form.js',
      ),
      'css' => array(
        'zen-form.css',
      ),
    ),
  );

  return $libraries;
}

/**
 * Implements hook_form_alter().
 *
 * @todo: Add zen mode for single elements.
 */
function zen_form_form_alter(&$form, &$form_state, $form_id) {
  $regex = _zen_form_get_form_ids_regex();

  if (preg_match($regex, $form_id)) {

    // Add zen for all supported elements in form.
    $form['#attributes'] = array(
      'class' => array('zen-mode'),
    );

    _zen_form_add_plugin();
  }
}

/**
 * Implements hook_zen_form_form_ids().
 */
function zen_form_zen_form_form_ids() {
  return array(
    '_node_form',
  );
}

/**
 * Adds zen plugin and other required js/css/settings.
 */
function _zen_form_add_plugin() {
  // Avoid adding plugin more than once.
  $plugin_added = & drupal_static(__FUNCTION__, FALSE);

  if (!$plugin_added) {
    // Load the 'Zen Form' js plugin library.
    libraries_load('zen_form_plugin');

    // Add module js, which handles attachment of plugin.
    $module_path = drupal_get_path('module', 'zen_form');
    drupal_add_js("{$module_path}/js/zen_form.attach.js");

    // Define settings for module js and plugin.
    $settings = array(
      'goButtonText' => t('Open form in zen mode'),
    );

    // Add module js and plugin settings.
    drupal_add_js(array('zenForm' => $settings), 'setting');

    $plugin_added = TRUE;
  }
}

/**
 * Collects form ids that will have zen mode enabled.
 *
 * @return array
 *   Form id strings, to be used in regex.
 */
function _zen_form_get_form_ids() {
  $form_ids = module_invoke_all('zen_form_form_ids');

  drupal_alter('zen_form_form_ids', $form_ids);

  return $form_ids;
}

/**
 * Returns form ids flattened to a regex string.
 *
 * @return string
 *   Regex string containing grouped form ids separated with OR.
 */
function _zen_form_get_form_ids_regex() {
  $form_ids = _zen_form_get_form_ids();
  $grouped_form_ids = array();

  // Group form id strings for regex.
  foreach ($form_ids as $form_id) {
    $grouped_form_ids[] = "({$form_id})";
  }

  // Glue grouped form ids together with OR regex operator.
  $form_ids_flattened = implode('|', $grouped_form_ids);

  // Return as regex string.
  return "/{$form_ids_flattened}/";
}
